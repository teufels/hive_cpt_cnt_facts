<?php
defined('TYPO3_MODE') or die();

$extKey = 'hive_cpt_cnt_facts';
$title = '[HIVE] Facts';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', $title);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntfacts_domain_model_fact', 'EXT:hive_cpt_cnt_facts/Resources/Private/Language/locallang_csh_tx_hivecptcntfacts_domain_model_fact.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntfacts_domain_model_fact');