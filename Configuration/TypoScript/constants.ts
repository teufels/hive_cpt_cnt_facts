
plugin.tx_hivecptcntfacts_hivecptcntfactsfactrenderfact {
    view {
        # cat=plugin.tx_hivecptcntfacts_hivecptcntfactsfactrenderfact/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_facts/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntfacts_hivecptcntfactsfactrenderfact/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_facts/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntfacts_hivecptcntfactsfactrenderfact/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_facts/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntfacts_hivecptcntfactsfactrenderfact//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin.tx_hivecptcntfacts {
    settings {
        tSeperator = .
        dSeperator = ,
        useEasing = 1
    }
}