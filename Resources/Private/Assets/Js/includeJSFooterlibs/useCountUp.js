var hive_cpt_cnt_facts_interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {

    }  else {

        if (typeof hive_cfg_typoscript__windowLoad == 'boolean' && hive_cfg_typoscript__windowLoad) {
            clearInterval(hive_cpt_cnt_facts_interval);

            // FKT isOnScreen
            $.fn.isOnScreen = function(){

                var win = $(window);

                var viewport = {
                    top : win.scrollTop(),
                    left : win.scrollLeft()
                };
                viewport.right = viewport.left + win.width();
                viewport.bottom = viewport.top + win.height();

                var bounds = this.offset();
                // console.log(bounds);

                bounds.right = bounds.left + this.outerWidth();
                bounds.bottom = bounds.top + this.outerHeight();

                return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

            };


            var oCountUpFacts = $('.hive-cpt-cnt-facts .count-up--value');
            if (oCountUpFacts.length > 0) {
                var i = 1;
                oCountUpFacts.each(function (e) {


                    var iTargetElementId = $(this).attr('id');
                    var iStartValue = $(this).attr('data-start');
                    var iEndValue = $(this).attr('data-target');
                    var iDecimals = $(this).attr('data-decimals');
                    var iDuration = $(this).attr('data-duration');
                    var sSeperator = $(this).attr('data-seperator');
                    var sDecimal = $(this).attr('data-decimal-seperator');
                    var sPrefix = $(this).attr('data-prefix');
                    var sSuffix = $(this).attr('data-suffix');
                    var bUseEasing = ($(this).attr('data-easing') == '1');
                    var bUseGrouping = ($(this).attr('data-thousands-separator') == '1');

                    var options = {
                        useEasing: bUseEasing,
                        useGrouping: bUseGrouping,
                        separator: sSeperator,
                        decimal: sDecimal,
                        prefix: sPrefix,
                        suffix: sSuffix
                    };

                    var factCount = new CountUp(iTargetElementId, iStartValue, iEndValue, iDecimals, iDuration, options);
                    // console.log(factCount);



                    if ($('#'+iTargetElementId).isOnScreen()) {
                        setTimeout(function() {
                            factCount.start();
                        }, 500);
                    } else {
                        $(window).scroll(function(){
                            if ($('#'+iTargetElementId).isOnScreen()) {
                                setTimeout(function() {
                                    factCount.start();
                                }, 500);
                            }
                        });
                    }
                    i++;
                });
            }
        }

    }

}, 2000);