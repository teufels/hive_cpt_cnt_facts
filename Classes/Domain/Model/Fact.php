<?php
namespace HIVE\HiveCptCntFacts\Domain\Model;

/***
 *
 * This file is part of the "hive_cpt_cnt_facts" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Fact
 */
class Fact extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * subTitle
     *
     * @var string
     */
    protected $subTitle = '';

    /**
     * startValue
     *
     * @var float
     */
    protected $startValue = 0.0;

    /**
     * value
     *
     * @var float
     */
    protected $value = 0.0;

    /**
     * valuePrefix
     *
     * @var string
     */
    protected $valuePrefix = '';

    /**
     * valueSuffix
     *
     * @var string
     */
    protected $valueSuffix = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade remove
     */
    protected $image = null;

    /**
     * decimals
     *
     * @var int
     */
    protected $decimals = 0;

    /**
     * useThousandsSeperator
     *
     * @var bool
     */
    protected $useThousandsSeperator = false;

    /**
     * duration
     *
     * @var int
     */
    protected $duration = 0;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the subTitle
     *
     * @return string $subTitle
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Sets the subTitle
     *
     * @param string $subTitle
     * @return void
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;
    }

    /**
     * Returns the value
     *
     * @return float $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the value
     *
     * @param float $value
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Returns the valuePrefix
     *
     * @return string $valuePrefix
     */
    public function getValuePrefix()
    {
        return $this->valuePrefix;
    }

    /**
     * Sets the valuePrefix
     *
     * @param string $valuePrefix
     * @return void
     */
    public function setValuePrefix($valuePrefix)
    {
        $this->valuePrefix = $valuePrefix;
    }

    /**
     * Returns the valueSuffix
     *
     * @return string $valueSuffix
     */
    public function getValueSuffix()
    {
        return $this->valueSuffix;
    }

    /**
     * Sets the valueSuffix
     *
     * @param string $valueSuffix
     * @return void
     */
    public function setValueSuffix($valueSuffix)
    {
        $this->valueSuffix = $valueSuffix;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the duration
     *
     * @return int $duration
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Sets the duration
     *
     * @param int $duration
     * @return void
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * Returns the startValue
     *
     * @return float $startValue
     */
    public function getStartValue()
    {
        return $this->startValue;
    }

    /**
     * Sets the startValue
     *
     * @param float $startValue
     * @return void
     */
    public function setStartValue($startValue)
    {
        $this->startValue = $startValue;
    }

    /**
     * Returns the decimals
     *
     * @return int $decimals
     */
    public function getDecimals()
    {
        return $this->decimals;
    }

    /**
     * Sets the decimals
     *
     * @param int $decimals
     * @return void
     */
    public function setDecimals($decimals)
    {
        $this->decimals = $decimals;
    }

    /**
     * Returns the boolean state of useThousandSeperator
     *
     * @return bool
     */
    public function isUseThousandSeperator()
    {
        return $this->useThousandSeperator;
    }

    /**
     * Returns the useThousandsSeperator
     *
     * @return bool useThousandsSeperator
     */
    public function getUseThousandsSeperator()
    {
        return $this->useThousandsSeperator;
    }

    /**
     * Sets the useThousandsSeperator
     *
     * @param bool $useThousandsSeperator
     * @return void
     */
    public function setUseThousandsSeperator($useThousandsSeperator)
    {
        $this->useThousandsSeperator = $useThousandsSeperator;
    }
}
