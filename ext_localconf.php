<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntFacts',
            'Hivecptcntfactsfactrenderfact',
            [
                'Fact' => 'renderFact'
            ],
            // non-cacheable actions
            [
                'Fact' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptcntfactsfactrenderfact {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('hive_cpt_cnt_facts') . 'Resources/Public/Icons/user_plugin_hivecptcntfactsfactrenderfact.svg
                        title = LLL:EXT:hive_cpt_cnt_facts/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_facts_domain_model_hivecptcntfactsfactrenderfact
                        description = LLL:EXT:hive_cpt_cnt_facts/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_facts_domain_model_hivecptcntfactsfactrenderfact.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntfacts_hivecptcntfactsfactrenderfact
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntfactsfactrenderfact >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecptcntfactsfactrenderfact {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Facts
                            description = reder Facts
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntfacts_hivecptcntfactsfactrenderfact
                            }
                        }
                        show := addToList(hivecptcntfactsfactrenderfact)
                    }

                }
            }'
        );

    }, $_EXTKEY
);