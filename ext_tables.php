<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntFacts',
            'Hivecptcntfactsfactrenderfact',
            'hive_cpt_cnt_facts :: Fact :: renderFact'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_facts', 'Configuration/TypoScript', 'hive_cpt_cnt_facts');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntfacts_domain_model_fact', 'EXT:hive_cpt_cnt_facts/Resources/Private/Language/locallang_csh_tx_hivecptcntfacts_domain_model_fact.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntfacts_domain_model_fact');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
